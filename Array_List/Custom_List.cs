﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Array_List
{
    public class CustomList<T> : IList<T>
    {

        private Item<T> head;
        private  Item<T> tail;

        public int Count { get; private set;}

        public bool IsReadOnly => throw new NotImplementedException();

        public CustomList(params T[] values)
        {
            foreach (var item in values)
            {
                this.Add(item);
            }
        }

        public CustomList(IEnumerable<T> values)
        {
            foreach (var item in values)
            {
                this.Add(item);
            }
        }

        public T this[int item]
        {
            get
            {
                if (item > Count - 1 || item < 0)
                    throw new IndexOutOfRangeException();

                var current = head;
                for (int i = 0; i < item; i++)
                {
                    current = current.Next;
                }

                return current.Data;
            }
            set
            {
                if (item > Count - 1 || item < 0)
                    throw new IndexOutOfRangeException();

                var current = head;
                for (int i = 0; i < item; i++)
                {
                    current = current.Next;
                }

                current.Data = value;
            }
        }

        public void Add(T data)
        {
            if (data == null)
                throw new ArgumentNullException(nameof(data));

            Item<T> item = new Item<T>(data);

            if (head == null)
            {
                head = item;
                tail = item;
            }
            else
                tail.Next = item;

            tail = item;
            Count++;
        }

        public void Clear()
        {
            head = null;
            tail = null;
            Count = 0;
        }

        public bool Contains(T item)
        {
            var current = head;

            while(current != null)
            {
                if (current.Data.Equals(item))
                    return true;

                current = current.Next;
            }

            return false;
        }

        public bool Remove(T item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            Item<T> previous = null;
            var current = head;

            while (current != null)
            {
                if (current.Data.Equals(item))
                {
                    if (previous != null)
                    {
                        previous.Next = current.Next;

                        if (current.Next == null)
                            tail = previous;
                    }
                    else
                    {
                        head = head.Next;

                        if (head == null)
                            tail = null;
                    }

                    Count--;
                    return true;
                }

                previous = current;
                current = current.Next;
            }

            return false;
        }

        public int IndexOf(T item)
        {
            var current = head;
            int index = 0;

            while (current != null)
            {
                if (current.Data.Equals(item))
                {
                    return index;
                }

                current = current.Next;
                index++;
            }
            return -1;
        }

        public void Insert(int index, T item)
        {
            if (index < 0 || index > Count)
                throw new IndexOutOfRangeException();

            Item<T> newItem = new Item<T>(item);

            var current = head;
            Item<T> previous = null;

            for (int i = 0; i < index; i++)
            {
                previous = current;
                current = current.Next;
            }

            if(previous == null)
            {
                var oldItem = head;
                head = newItem;
                newItem.Next = oldItem;
            }
            else
            {
                newItem.Next = current;
                previous.Next = newItem;
            }

            Count++;
        }

        public void RemoveAt(int index)
        {
            if (index < 0 || index > Count - 1)
                throw new IndexOutOfRangeException();

            var current = head;
            Item<T> previous = null;
           
            for (int i = 0; i < index; i++)
            {
                previous = current;
                current = current.Next;
            }

            if(current == head)
            {
                head = head.Next;
                if (head == null)
                    tail = null;
            }
            else
            {
                if (current == tail)
                    tail = previous;
              
                previous.Next = current.Next;
            }
            Count--;
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            var current = head;

            while (current != null)
            {
                array[arrayIndex] = current.Data;
                arrayIndex++;
                current = current.Next;
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            var current = head;

            while (current != null)
            {
                yield return current.Data;
                current = current.Next;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)this).GetEnumerator();
        }

    }
}
